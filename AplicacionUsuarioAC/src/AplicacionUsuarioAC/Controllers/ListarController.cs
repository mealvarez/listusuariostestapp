﻿using AplicacionUsuarioAC.Reposity.Listar;
using Microsoft.AspNetCore.Mvc;
using AplicacionUsuarioAC.Models;
using System;

namespace AplicacionUsuarioAC.Controllers
{
    public class ListarController : Controller
    {
        UsuariosRepository _usuariosRepository;
        
        public IActionResult Usuarios()
        {
            _usuariosRepository = new UsuariosRepository();
            var model = _usuariosRepository.DameLosUsuarios();
            return View(model);
        }

        public IActionResult AltaUsuario()
        {
            return View();
        }

        public IActionResult AgregarUsuario(string nombre, string email, string pais, bool estado)
        {
            var model = new Usuario()
            {
                Nombre = nombre,
                Email = email,
                Pais = pais,
                FechaUltimoLogin = DateTime.Now,
                Estado = estado
            };
            return PartialView("_usuario", model);
        }
    }
}
