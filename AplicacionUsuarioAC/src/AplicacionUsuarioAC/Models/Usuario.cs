﻿using System;

namespace AplicacionUsuarioAC.Models
{
    public class Usuario
    {
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Pais { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaUltimoLogin { get; set; }
         
    }
}
