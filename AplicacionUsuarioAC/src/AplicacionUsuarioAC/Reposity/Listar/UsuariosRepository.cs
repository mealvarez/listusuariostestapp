﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AplicacionUsuarioAC.Models;

namespace AplicacionUsuarioAC.Reposity.Listar
{
    public class UsuariosRepository
    {

        public IList<Usuario> DameLosUsuarios()
        {
            return new List<Usuario>()
            {
                new Usuario()
                {
                    Email = "unchavoncito@persona.com",
                    Estado = true,
                    FechaUltimoLogin = DateTime.Now,
                    Nombre = "elbraian",
                    Pais = "Argentina"
                },
                new Usuario()
                {
                    Email = "otrochavoncito@persona.com",
                    Estado = false,
                    FechaUltimoLogin = new DateTime(2016,5,25),
                    Nombre = "elmartin",
                    Pais = "Uruguay"
                },
                new Usuario()
                {
                    Email = "otrochavoncitomas@nopersona.com",
                    Estado = true,
                    FechaUltimoLogin = new DateTime(2016,3,25),
                    Nombre = "lashani",
                    Pais = "Argelia"
                },
                new Usuario()
                {
                    Email = "elultimochavoncito@nopersona.com",
                    Estado = false,
                    FechaUltimoLogin = new DateTime(2016,2,25),
                    Nombre = "larakel",
                    Pais = "Irak"
                }
            };
        } 
    }
}
