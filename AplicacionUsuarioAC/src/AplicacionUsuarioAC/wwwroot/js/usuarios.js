﻿window.onload = function () {
    document.getElementById("filtrar").addEventListener("keyup", filtrarUsuarios);

    $(".agregar").click(function () {
        $("#modal").load("AltaUsuario", function () {
            $("#save").click(Aceptar);
            $("#modal").modal("show");
        });
    });

    $(".eliminar").click(function () {
        $(this).parent().parent().remove();
    })

}


function filtrarUsuarios() {
    _this = this;

    $.each($("#tablaUsuarios tbody tr"), function () {
        if ($(this).attr("name").toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
            $(this).hide();
        else
            $(this).show();
    });
}

function Aceptar() {
    //validaciones
    var data = $("#nuevo-usuario").serialize();
    var url = "/Listar/AgregarUsuario?" + data;
    $.get(url, function (response) {
        $("#tablaUsuarios tbody").prepend(response);
    });
    $("#modal").modal("toggle");
};